## HTTP-SERVER
Aplicacion de peticiones

## Install
npm install

## Use in development
nodemon server/server.js

## Preview
https://blooming-basin-10047.herokuapp.com/

## DOC en Postman de la API
https://documenter.getpostman.com/view/11696857/Szzn7x6w?version=latest

##  Creator of the login_demo project
👤 **Sergio V.O**
* Twitter:https://twitter.com/smavo24
* Github: https://github.com/smavo
* Gitlab: [@smavo] (https://gitlab.com/smavo)
* LinkedIn: https://www.linkedin.com/in/smavo24/
* Platzi: https://platzi.com/@sergio-vo/
